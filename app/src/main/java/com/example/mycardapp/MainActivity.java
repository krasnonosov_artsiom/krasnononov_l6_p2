package com.example.mycardapp;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.DiscretePathEffect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.checks.CheckInputClass;
import com.example.database.Contact;
import com.example.database.DatabaseHandler;
import com.example.dialogs.DialogCreator;
import com.example.utils.Utils;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    public static final int EXPLORER_CODE = 1;
    public static final int CAMERA_CODE = 2;
    public static final String SAVE_PATH = "/storage/emulated/0/Android/data/com.mycard";
    public static final String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};

    String pathToFile;
    EditText nameEditText;
    EditText phoneEditText;
    EditText emailEditText;
    ImageView imageView;

    Button callButton;
    Button emailButton;
    ImageButton contactsButton;
    ImageButton contactsInFilesButton;
    Menu menu;

    int currentId = -1;
    boolean flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_action_bar);

        nameEditText = findViewById(R.id.nameEditText);
        phoneEditText = findViewById(R.id.phoneEditText);
        emailEditText = findViewById(R.id.emailEditText);
        imageView = findViewById(R.id.savedUserImageView);

        callButton = findViewById(R.id.callButton);
        emailButton = findViewById(R.id.emailButton);
        contactsButton = findViewById(R.id.contactsButton);
        contactsInFilesButton = findViewById(R.id.contactsInFilesButton);
        imageView.setClickable(false);

        Intent intent = getIntent();
        intent.getStringExtra(Utils.KEY_PHOTO_PATH);

        if (intent.getStringExtra(Utils.KEY_PHOTO_PATH) != null) {
            currentId = intent.getIntExtra(Utils.KEY_ID, -1);
            nameEditText.setText(intent.getStringExtra(Utils.KEY_NAME));
            phoneEditText.setText(intent.getStringExtra(Utils.KEY_NUMBER));
            emailEditText.setText(intent.getStringExtra(Utils.KEY_EMAIL));
            Bitmap photo = BitmapFactory.decodeFile(intent.getStringExtra(Utils.KEY_PHOTO_PATH));
            imageView.setImageBitmap(photo);
        }

        if (pathToFile != null) {
            Bitmap photo = BitmapFactory.decodeFile(intent.getStringExtra(Utils.KEY_PHOTO_PATH));
            imageView.setImageBitmap(photo);
        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_menu_main_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        AlertDialog.Builder adbPos = new AlertDialog.Builder(this);
        adbPos.setTitle("Make your choice");
        adbPos.setCancelable(false);
        adbPos.setPositiveButton("Save to DataBase", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveContactToDB();
            }
        });
        adbPos.setNegativeButton("Save as file", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveToFile();
            }
        });

        AlertDialog.Builder adbNeg = new AlertDialog.Builder(this);
        adbNeg.setTitle("Error!");
        adbNeg.setMessage("Contact can't be added because of you entered unacceptable data");
        adbNeg.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        switch (item.getItemId()) {

            case R.id.editMenu:
                animationOut();
                imageView.setClickable(false);
                flag = false;
                break;

            case R.id.addMenu:
                animationOut();
                nameEditText.setText("");
                phoneEditText.setText("");
                emailEditText.setText("");
                imageView.setClickable(true);
                imageView.setImageResource(R.drawable.default_photo_picture);
                flag = true;
                break;

            case R.id.saveMenu:
                callButton.animate().translationX(0).setDuration(500);
                emailButton.animate().translationX(0).setDuration(500);
                contactsButton.animate().translationX(0).setDuration(500);
                contactsInFilesButton.animate().translationX(0).setDuration(500);
                menu.findItem(R.id.editMenu).setVisible(true);
                menu.findItem(R.id.addMenu).setVisible(true);
                menu.findItem(R.id.saveMenu).setVisible(false);
                nameEditText.setEnabled(false);
                phoneEditText.setEnabled(false);
                emailEditText.setEnabled(false);
                imageView.setClickable(false);
                if (flag) {

                    if (checkingInput(phoneEditText, "phone") && checkingInput(emailEditText, "email")) {
                        adbPos.show();
                    } else {
                        if (pathToFile != null) {
                            deletePhoto();
                        }
                        adbNeg.show();
                    }

                } else {
                    if (checkingInput(phoneEditText, "phone") && checkingInput(emailEditText, "email")) {
                        updateContactInDB();
                    } else {
                        if (pathToFile != null) {
                            deletePhoto();
                        }
                        adbNeg.show();
                    }
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void deletePhoto() {
        File file = new File(pathToFile);
        file.delete();
    }

    private boolean checkingInput(EditText editText, String key) {

        boolean inputFlag = true;
        String checkLine = editText.getText().toString();
        CheckInputClass checkInput = new CheckInputClass();
        switch (key) {
            case "phone" :
                inputFlag = checkInput.checkPhone(checkLine);
                break;
            case "email" :
                inputFlag = checkInput.checkEmail(checkLine);
                break;
        }

        return inputFlag;
    }

    private void animationOut() {
        callButton.animate().translationX(-1500).setDuration(500);
        emailButton.animate().translationX(1500).setDuration(500);
        contactsButton.animate().translationX(1500).setDuration(500);
        contactsInFilesButton.animate().translationX(1500).setDuration(500);
        menu.findItem(R.id.editMenu).setVisible(false);
        menu.findItem(R.id.addMenu).setVisible(false);
        menu.findItem(R.id.saveMenu).setVisible(true);
        nameEditText.setEnabled(true);
        phoneEditText.setEnabled(true);
        emailEditText.setEnabled(true);
    }

    public void sendByEmail(View view) {

        final String email = emailEditText.getText().toString();
        if (email.equals("")) {
            Toast.makeText(getApplicationContext(), "Email field must be not empty", Toast.LENGTH_SHORT).show();
            return;
        }

        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Select an action");
        adb.setNegativeButton("Send by email", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:" + email));
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });

        adb.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        adb.show();

    }

    public void phone(View view) {

        if (phoneEditText.getText().toString().equals("")) {
            Toast.makeText(this, "Phone field must be not empty", Toast.LENGTH_SHORT).show();
            return;
        }

        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Select an action ");
        adb.setNegativeButton("Call this contact", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String phoneNumber = "tel:" + phoneEditText.getText().toString();

                if (checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse(phoneNumber));
                    startActivity(intent);
                } else {
                    if (shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {
                        Toast.makeText(getApplicationContext(), "Required additional permission on \"phone\" app", Toast.LENGTH_SHORT).show();
                    }
                    requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 2);
                }
            }
        });

        adb.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        adb.show();

    }

    public void openContactList(View view) {

        Intent intent = new Intent(this, ListView.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_to_left, R.anim.slide_out_right);

    }

    private void makePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_CODE);
    }

    public void openFileExplorer(View view) {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        Uri uri = Uri.parse(String.valueOf(getApplicationContext().getExternalFilesDir("com.mycard")));
        intent.setDataAndType(uri, "*/*");
        try {
            startActivityForResult(intent, EXPLORER_CODE);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "There are no file explorer clients installed.", Toast.LENGTH_SHORT).show();
        }

//        DialogCreator creator = new DialogCreator("Choose something", null, null, "Cancel", null, null);
//        creator.rightButtonListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(getApplicationContext(), "You push right button", Toast.LENGTH_SHORT).show();
//            }
//        });
//        creator.show(getSupportFragmentManager(), "TAG");

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode == CAMERA_CODE) {

            if (resultCode == RESULT_OK) {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                imageView.setImageBitmap(photo);

                new File(SAVE_PATH).mkdir();
                pathToFile = savePhotoOnStorage(photo);
            }

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

        if (requestCode == EXPLORER_CODE) {

            String path = data.getData().getPath();
            int index = path.indexOf(":") + 1;
            path = Environment.getExternalStorageDirectory() + "/" + path.substring(index);
            File file = new File(path);

            try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                String json = reader.readLine();
                Contact contact = new Gson().fromJson(json, Contact.class);
                nameEditText.setText(contact.getName());
                phoneEditText.setText(contact.getPhoneNumber());
                emailEditText.setText(contact.getEmail());
                if (!contact.getPathToPhoto().equals("")) {
                    Bitmap photo = BitmapFactory.decodeFile(contact.getPathToPhoto());
                    imageView.setImageBitmap(photo);
                }
            } catch (IOException e) {
                Toast.makeText(this, "IOException", Toast.LENGTH_SHORT).show();
            }

        }
    }

    public void checkPermissions(View view) {

        if (condition()) {
            makePhoto();
        } else {
            if (shouldShowRequestPermissionRationale(permissions[0])) {
                Toast.makeText(this, "Required additional permission on camera", Toast.LENGTH_SHORT).show();
            } else if (shouldShowRequestPermissionRationale(permissions[1]) ||
                    shouldShowRequestPermissionRationale(permissions[2])) {
                Toast.makeText(this, "Required additional permission on storage", Toast.LENGTH_SHORT).show();
            }

            requestPermissions(permissions, 1);
        }
    }

    private boolean condition() {
        return checkSelfPermission(permissions[0]) == PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(permissions[1]) == PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(permissions[2]) == PackageManager.PERMISSION_GRANTED;
    }

    private String savePhotoOnStorage(Bitmap photo) {

        int counter = 1;
        String pathWithPhotoName;

        /*
        checking on existing files
         */
        while (true) {
            StringBuilder photoNameBuilder = new StringBuilder(SAVE_PATH + "/Photo" + counter + ".PNG");
            if (new File(photoNameBuilder.toString()).exists()) {
                counter++;
            } else {
                pathWithPhotoName = photoNameBuilder.toString();
                break;
            }
        }

        /*
        saving files
         */
        try (FileOutputStream fos = new FileOutputStream(pathWithPhotoName)) {
            photo.compress(Bitmap.CompressFormat.PNG, 80, fos);
            fos.flush();
        } catch (FileNotFoundException e) {
            Toast.makeText(this, "FileNotFoundException", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(this, "IOException", Toast.LENGTH_SHORT).show();
        }

        return pathWithPhotoName;
    }

    private void saveContactToDB() {
        String name = nameEditText.getText().toString();
        String phone = phoneEditText.getText().toString();
        String email = emailEditText.getText().toString();

        DatabaseHandler handler = new DatabaseHandler(this);
        handler.addContact(new Contact(name, phone, email, pathToFile));
    }

    private void updateContactInDB() {
        DatabaseHandler dbh = new DatabaseHandler(this);
        Contact contact = dbh.getContact(currentId);
        contact.setName(nameEditText.getText().toString());
        contact.setPhoneNumber(phoneEditText.getText().toString());
        contact.setEmail(emailEditText.getText().toString());
        dbh.updateContact(contact);
    }

    private void saveToFile() {
        String name = nameEditText.getText().toString();
        String phone = phoneEditText.getText().toString();
        String email = emailEditText.getText().toString();

        Contact contact = new Contact(name, phone, email, pathToFile);
        String json = new Gson().toJson(contact);

        new File(SAVE_PATH).mkdir();
        String pathToSavedFiles = SAVE_PATH + "/textfiles/";
        new File(pathToSavedFiles).mkdir();

        int counter = 1;
        while (true) {
            StringBuilder textContactBuilder = new StringBuilder(pathToSavedFiles + "/contact" + counter + ".TXT");
            if (new File(textContactBuilder.toString()).exists()) {
                counter++;
            } else {
                pathToSavedFiles = textContactBuilder.toString();
                break;
            }
        }

        File file = new File(pathToSavedFiles);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(json);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}