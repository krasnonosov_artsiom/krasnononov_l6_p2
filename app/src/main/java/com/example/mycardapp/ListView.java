package com.example.mycardapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.adapters.Adapter;
import com.example.adapters.SwipeController;
import com.example.database.Contact;
import com.example.database.DatabaseHandler;

import java.util.ArrayList;

public class ListView extends AppCompatActivity {

    RecyclerView recyclerView;
    Adapter adapter;
    RecyclerView.LayoutManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        recyclerView = findViewById(R.id.recyclerView);

        ArrayList<Contact> contacts = (ArrayList<Contact>) new DatabaseHandler(this).getAllContacts();

        adapter = new Adapter(contacts, this);
        manager = new LinearLayoutManager(this);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(manager);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new SwipeController(adapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);


    }
}
