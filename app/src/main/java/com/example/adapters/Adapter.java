package com.example.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.telecom.Call;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.example.database.Contact;
import com.example.database.DatabaseHandler;
import com.example.mycardapp.ListView;
import com.example.mycardapp.MainActivity;
import com.example.mycardapp.R;
import com.example.utils.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    private ArrayList<Contact> contacts;
    Context context;

    public Adapter(ArrayList<Contact> contacts, Context context) {
        this.contacts = contacts;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView photo;
        TextView name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            photo = itemView.findViewById(R.id.itemImageView);
            name = itemView.findViewById(R.id.itemNameTextView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Contact contact = contacts.get(getAdapterPosition());
            String name = contact.getName();
            String phone = contact.getPhoneNumber();
            String email = contact.getEmail();
            String photoPath = contact.getPathToPhoto();

            Intent intent = new Intent(context, MainActivity.class);
            intent.putExtra(Utils.KEY_ID, contact.getId());
            intent.putExtra(Utils.KEY_NAME, name);
            intent.putExtra(Utils.KEY_NUMBER, phone);
            intent.putExtra(Utils.KEY_EMAIL, email);
            intent.putExtra(Utils.KEY_PHOTO_PATH, photoPath);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);and
            Activity activity = (Activity) context;
            activity.startActivity(intent);
            activity.overridePendingTransition(R.anim.slide_to_left, R.anim.slide_out_right);

        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Contact contact = contacts.get(position);

        holder.name.setText(contact.getName());
        holder.photo.setImageBitmap(BitmapFactory.decodeFile(contact.getPathToPhoto()));
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    public void deleteItem(int position) {
        DatabaseHandler dbh = new DatabaseHandler(context);

        Contact contact = contacts.get(position);
        dbh.deleteContact(contact);
        if (contact.getPathToPhoto() != null) {
            File file = new File (contact.getPathToPhoto());
            file.delete();
        }
    }

    
}
