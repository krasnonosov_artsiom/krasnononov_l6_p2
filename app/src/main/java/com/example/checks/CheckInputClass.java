package com.example.checks;

public class CheckInputClass {

    public boolean checkPhone (String phone) {
        char[] phoneChars = phone.toCharArray();
        for (int i = 0; i < phoneChars.length; i++) {
            if (phoneChars[i] < '0' && phoneChars[i] > '9' && phoneChars[i] != '+') {
                return false;
            }
        }
        if (phone.contains("+") && !phone.startsWith("+")) {
            return false;
        }
        if (phone.length() > 15) {
            return false;
        }
        return true;
    }

    public boolean checkEmail (String email) {
        if (!email.contains("@")) {
            return false;
        }
        if (!email.endsWith(".ru") && email.endsWith(".by") && email.endsWith(".com") && email.endsWith(".net")
                && email.endsWith(".ua") && email.endsWith(".de")) {
            return false;
        }
        return true;
    }

}
