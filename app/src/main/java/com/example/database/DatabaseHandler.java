package com.example.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.core.content.ContextCompat;

import com.example.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {


    public DatabaseHandler(Context context) {
        super(context, Utils.DATABASE_NAME, null, Utils.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " + Utils.TABLE_NAME + " (" +
                Utils.KEY_ID + " INTEGER PRIMARY KEY, " +
                Utils.KEY_NAME + " TEXT, " +
                Utils.KEY_NUMBER + " TEXT, " +
                Utils.KEY_EMAIL + " TEXT, " +
                Utils.KEY_PHOTO_PATH + " TEXT)";
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Utils.TABLE_NAME);
        onCreate(db);
    }

    public void addContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(Utils.KEY_NAME, contact.getName());
        contentValues.put(Utils.KEY_NUMBER, contact.getPhoneNumber());
        contentValues.put(Utils.KEY_EMAIL, contact.getEmail());
        contentValues.put(Utils.KEY_PHOTO_PATH, contact.getPathToPhoto());

        db.insert(Utils.TABLE_NAME, null, contentValues);
        db.close();
    }

    public Contact getContact(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(Utils.TABLE_NAME, new String[] {Utils.KEY_ID, Utils.KEY_NAME, Utils.KEY_NUMBER, Utils.KEY_EMAIL, Utils.KEY_PHOTO_PATH},
                Utils.KEY_ID + "=?", new String[] {String.valueOf(id)}, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        Contact contact = new Contact(Integer.parseInt(cursor.getString(0)), cursor.getString(1),
                cursor.getString(2), cursor.getString(3),cursor.getString(4));
        return contact;
    }

    public List<Contact> getAllContacts () {
         SQLiteDatabase db = this.getReadableDatabase();
         List<Contact> contacts = new ArrayList<>();

         String selectAllCars = "SELECT * FROM " + Utils.TABLE_NAME;
         Cursor cursor = db.rawQuery(selectAllCars, null);

         if (cursor.moveToFirst()) {
             do {
                 Contact contact = new Contact(Integer.parseInt(cursor.getString(0)), cursor.getString(1),
                         cursor.getString(2), cursor.getString(3),cursor.getString(4));
                 contacts.add(contact);
             } while (cursor.moveToNext());
         }

         return contacts;
    }

    public void deleteContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(Utils.TABLE_NAME, Utils.KEY_ID + "=?", new String[] {String.valueOf(contact.getId())});
        db.close();
    }

    public int updateContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(Utils.KEY_ID, contact.getId());
        contentValues.put(Utils.KEY_NAME, contact.getName());
        contentValues.put(Utils.KEY_NUMBER, contact.getPhoneNumber());
        contentValues.put(Utils.KEY_EMAIL, contact.getEmail());
        contentValues.put(Utils.KEY_PHOTO_PATH, contact.getPathToPhoto());

        return db.update(Utils.TABLE_NAME, contentValues, Utils.KEY_ID + "=?", new String[] {String.valueOf(contact.getId())});
    }

}
