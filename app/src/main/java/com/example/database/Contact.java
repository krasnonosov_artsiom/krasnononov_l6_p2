package com.example.database;

public class Contact {

    private int id;
    private String name;
    private String phoneNumber;
    private String email;
    private String pathToPhoto;

    public Contact(int id, String name, String phoneNumber, String email, String pathToPhoto) {
        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.pathToPhoto = pathToPhoto;
    }

    public Contact(String name, String phoneNumber, String email, String pathToPhoto) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.pathToPhoto = pathToPhoto;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public String getPathToPhoto() {
        return pathToPhoto;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPathToPhoto(String pathToPhoto) {
        this.pathToPhoto = pathToPhoto;
    }

    @Override
    public String toString() {
        return getClass() + " name: " + name + ", phone: " + phoneNumber + ", email: " + email;
    }
}
