package com.example.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.example.mycardapp.R

class DialogCreator(private val titleText: String, private val messageText: String?, private val editTextHint: String?,
                             private val rightButtonText: String, private val middleButtonText: String?,
                    private val leftButtonText: String?) : DialogFragment() {

    //widgets
    private lateinit var title: TextView
    private lateinit var message: TextView
    private lateinit var dialogEditText: EditText
    private lateinit var rightButton: Button
    private lateinit var middleButton: Button
    private lateinit var leftButton: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_layout, container, false)
        title = view.findViewById(R.id.titleTextView)
        title.text = titleText

        message = view.findViewById(R.id.messageTextView)
        if (messageText != null) {
            message.visibility = VISIBLE
            message.text = messageText
        }

        dialogEditText = view.findViewById(R.id.dialogEditText)
        if (editTextHint != null) {
            dialogEditText.visibility = VISIBLE
            dialogEditText.hint = editTextHint
        }

        rightButton = view.findViewById(R.id.rightButton)
        rightButton.text = rightButtonText
        rightButton

        middleButton = view.findViewById(R.id.middleButton)
        if (middleButtonText != null) {
            middleButton.text = middleButtonText
        }

        leftButton = view.findViewById(R.id.leftButton)
        if (leftButtonText != null) {
            leftButton.text = leftButtonText
        }
        return view
    }

}