package com.example.utils;

public class Utils {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "contactsDB";
    public static final String TABLE_NAME = "contacts";

    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_NUMBER = "number";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_PHOTO_PATH = "pathPhoto";


}
